###############################################################################
# Semester 2 2017 Assignment #2 - September - October 2017                    #
# Project completed by : Cameron Watt and Bryan Chu Chait Soo                 #
# Student Number       : s3589163 and s3441433                                #
# Course Code          : COSC1254 - Programming using C++                     #
# Original Author      : Cameron Watt                                         #
# File Description     : 'Command' makefile. This file calls each of the      #
#                        'Slave' makefiles, before compiling the main file in #
#                        this directory, and finally linking all of the       #
#                        object files together                                #
###############################################################################

CC=g++

LFLAGS= 
CFLAGS= -Wall -pedantic -std=c++14 -g

BIN := draughts

all:
	############ COMPILE MODEL FILES WITHIN MODEL DIRECTORY ############
	+$(MAKE) -C model
	############ COMPILE VIEW FILES WITHIN VIEW DIRECTORY ############
	+$(MAKE) -C ncview
	############ COMPILE CONTROLLER FILES WITHIN CONTROLLER DIRECTORY ############
	+$(MAKE) -C nc_controller

	############ COMPILE MAIN FILE ############
	$(CC) -c main_noncurses.cpp $(CFLAGS)

	############ LINK ALL OBJECT FILES TOGETHER ############
	$(CC) ./model/*.o ./ncview/*.o ./nc_controller/*.o main_noncurses.o -o $(BIN) $(LFLAGS)

# Remove objects and binaries
clean:
		rm ./model/*.o
		rm ./ncview/*.o
		rm ./nc_controller/*.o
		rm  *.o $(BIN)

# Runs the script needed for RMIT servers
script:
	source /opt/rh/devtoolset-6/enable