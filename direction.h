/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Header file to describe a direction within the game  *
 ******************************************************************************/
#include <climits>
#pragma once
namespace draughts
{
    enum class direction
    {
        UP=-1, DOWN=1, BOTH=2, INVALID=INT_MAX
    };
}
