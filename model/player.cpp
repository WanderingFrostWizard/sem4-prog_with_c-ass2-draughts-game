/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Representation of a player in a draughts game        *
 ******************************************************************************/
#include "player.h"
#include "shared.h"

// Constructor
draughts::model::player::player( int id, std::string name ) 
		: playerID(id), playerName(name)
{
	gamesPlayed = 0;
	gamesWon = 0;
	gamesLost = 0;
	currentScore = 0;

	playerColor = NONE;
}

int draughts::model::player::getID( void )
{
	return playerID;
}

std::string draughts::model::player::getName( void )
{
	return playerName;
}

int draughts::model::player::getCurScore( void )
{
	return currentScore;
}

void draughts::model::player::incrementScore( int amount )
{
	currentScore = currentScore + amount;
}

void draughts::model::player::resetScore( void )
{
	currentScore = 0;
}

std::string draughts::model::player::getColor( void )
{
	if ( playerColor == RED )
		return COLOR_RED "RED" COLOR_RESET;
	else if ( playerColor == WHITE ) 
		return "WHITE";
	else
		return "ERROR IN playerColor";
}

void draughts::model::player::setColor( colors color )
{
	playerColor = color;
}