/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Representation of a player in a draughts game        *
 ******************************************************************************/
#include <string>

#pragma once

namespace draughts
{
    namespace model
    {
        class player
        {
            public:
            // color set to NONE when this player is NOT in a game
            // NOTE this needs to be public to allow model to use it
            enum colors { NONE, RED, WHITE };

            private:

            int playerID;
            std::string playerName;
            // FUTURE IMPLEMENTATIONS CAN USE THIS TO TRACK THE PLAYERS DATA
            int gamesPlayed, gamesWon, gamesLost;
            int currentScore;

            colors playerColor;

            public:
            player(int id, std::string name); 

            int getID( void );
            std::string getName( void );

            // Scoring functions
            int getCurScore( void );
            void incrementScore( int amount );
            void resetScore( void );

            std::string getColor( void );
            void setColor( colors color );
        };
    }
}