/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Representation of a single piece in a draughts game  *
 ******************************************************************************/
#include "piece.h"

#include <locale>	// for toupper();

// Constructor
draughts::model::piece::piece( char _token, int playerNo, int _direction )   
{  
	token = _token;
	crowned = false;
	ownerID = playerNo;
	direction = _direction;
}

// Return the piece's representative symbol generally r / R or w / W
char draughts::model::piece::get_symbol(void)
{
	if ( crowned )
		return toupper(token);
	else
		return token;
}

void draughts::model::piece::crown(void)
{
	crowned = true;
}

int draughts::model::piece::get_ownerID( void )
{
	return ownerID;
}

int draughts::model::piece::get_direction( void )
{
	return direction;
}

bool draughts::model::piece::check_crowned( void )
{
	return crowned;
}