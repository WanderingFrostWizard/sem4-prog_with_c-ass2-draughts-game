/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Used to represent the rules for the game of draughts *
 ******************************************************************************/
#include "model.h"
#include <iterator>
#include <string>

std::unique_ptr<draughts::model::model> draughts::model::model::instance =
nullptr;

draughts::model::model::model(void) 
{
    nextID = 0;
}

// Checks if the player exists NOTE it is private
bool draughts::model::model::player_exists(const std::string& pname)
{
    for ( std::vector<player *>::iterator it = playList.begin() ; it != playList.end(); ++it )
    {
        if ( (*it)->getName() == pname )
        {
            return true;
        }
    }
    return false;
}


// Adds a new player object to the playlist vector, IF the player hasn't 
// already been added
void draughts::model::model::add_player(const std::string& p)
{
    if ( player_exists( p ) )
    {
        throw std::string("ERROR: Player already exists");
    }
    else 
    {
        playList.push_back( new player( nextID, p ) );
        nextID += 1;
    }
}

std::string draughts::model::model::get_player_name(int id)
{
    for ( std::vector<player *>::iterator it = playList.begin() ; it != playList.end(); ++it )
    {
        if ( (*it)->getID() == id )
        {
            return (*it)->getName();
        }
    }
    return " ";
}

int draughts::model::model::get_player_score(int playernum)
{
    for ( std::vector<player *>::iterator it = playList.begin() ; it != playList.end(); ++it )
    {
        if ( (*it)->getID() == playernum )
        {
            return (*it)->getCurScore();
        }
    }
    return -1;
}

// Returns the integer ID of the current player
int draughts::model::model::get_current_player(void)
{
    return currentGame->get_current_player();
}

std::string draughts::model::model::get_player_color( int playernum )
{
    for ( std::vector<player *>::iterator it = playList.begin() ; it != playList.end(); ++it )
    {
        if ( (*it)->getID() == playernum )
        {
            return (*it)->getColor();
        }
    }
    return "ERROR Player NOT FOUND";
}

std::map<int, std::string> draughts::model::model::get_player_list(void) const
{
    std::map< int, std::string > tempList;

    for ( std::vector<player *>::const_iterator it = playList.begin() ; it != playList.end(); ++it )
    {
        tempList.insert(std::make_pair( (*it)->getID(), (*it)->getName() ));  
    }
    return tempList;
}

//TODO######################################
void draughts::model::model::start_game(int plr1, int plr2 )
{
    // Randomize red and white players //TODO#################################

    redPlayer = playList.at(  plr1 );
    whitePlayer = playList.at( plr2 );

    // Set the player color in the player objects
    redPlayer->setColor( player::colors::RED );
    whitePlayer->setColor( player::colors::WHITE );

    // Randomize starting player   //TODO#################################


    std::cout << "plr1 = " << plr1 << "  plr2 = " << plr2 << std::endl;  //TODO#################################

    currentGame = draughts::model::game::get_instance( plr1, plr2 );
}

// ??? This function NOT used ANYWHERE in the source code ???
int draughts::model::model::get_winner()
{
     //TODO ########################################################
    // Returns the winning player number to the calling function
    return EOF;
}

char draughts::model::model::get_token(int x ,int y)
{
    /* Using this reference, BECAUSE references to 
     * "currentGame->gameBoard[y][x] == NULL" WILL result in seg faults
     * even though we are checking for NULL references. However we can implement
     * the SAME function as above in game.cpp, without any issues */
    return currentGame->get_token_at( x , y );
}

// MOST OF THE GAME LOGIC is executed from this function
void draughts::model::model::make_move(int playernum,
        int startx, int starty, int endx, int endy)
{
    draughts::model::piece * tempPiece;

    if ( currentGame->get_piece( startx, starty ) == NULL )
    { 
        throw std::string("INVALID PIECE SELECTED");
    }
    else
    {
        // Store a temporary reference to the current piece, to avoid
        // repeating this call each time we want to access the current piece
        tempPiece = currentGame->get_piece( startx, starty );
    }

    // Check the START space
    if ( tempPiece->get_ownerID() != playernum )
    {
        throw std::string("THIS IS NOT YOUR PIECE");
    }
    else if ( ( startx == endx ) && ( starty == endy ) )
    {
        if ( captureStreak )
        {
            successfulMove = true;
        }
        else
        {
            throw std::string("CANNOT MOVE TO SAME SPACE");
        }
    }

    // Check the END space
    else if ( (endx + endy) % 2 != 0 )
    {
        throw std::string("MUST MOVE TO AN ODD SPACE");
    }
    else if ( currentGame->get_piece( endx, endy ) != NULL )
    {
        throw std::string("FINAL SPACE MUST BE EMPTY");
    }
    else  
    {
        // Calculate the MAGNITUDE of the DISPLACEMENT
        int dispx = startx - endx;
        if ( dispx < 0 )
            dispx = dispx * -1;

        int dispy = starty - endy;
        if ( dispy < 0 )
            dispy = dispy * -1;        

        /*
         * Calculate the direction the user is moving. NOTE this is the same as
         * the direction within the piece class, -1 if the player is moving 
         * from the bottom to the top, or 1 if they are moving downwards
         */
        int moveDir;
        if ( starty < endy )
            moveDir = -1;
        else 
            moveDir = 1;

        /* 
         * Check Direction
         * If the piece IS NOT crowned, and the direction is INCORRECT
         * If we multiply the piece direction with the direction the player is
         * trying to move it SHOULD REMAIN POSITIVE
         * ie  -1 * -1 = POS 1    or   1 * 1 = POS 1 Therefore CORRECT movement
         * but if the user is trying to move in the incorrect direction
         * ie  -1 *  1 = NEG 1    or   1 * -1 = NEG 1 Therefore INCORRECT move
         */
        if ( ( !tempPiece->check_crowned() ) && 
            ( tempPiece->get_direction() * moveDir > 0 ) )
        {
            throw std::string("PEG NOT CROWNED, MUST MOVE IN CORRECT DIRECTION");
        }
        else if ( dispx != dispy )
        {
            throw std::string("MUST MOVE IN A STRAIGHT LINE DIAGONALLY");
        }
        // ONLY NEED TO CHECK THE magnitude of one direction after the above check
        else if ( dispx > 2 )
        {
            throw std::string("PLEASE perform one attack at a time"); 
        }

         // If user has moved 1 square diagonally
        else if ( dispx == 1  ) // Standard move
        {
            if ( captureStreak )
            {
                throw std::string("You are on a captureStreak, you MUST capture for your next turn, or MOVE to the same space to end your turn"); 
            }
            else
            {
                // If the piece gets to the end, crown the piece
                if ( ( endy == 1 ) || ( endy == 8 ) )
                {
                    tempPiece->crown();
                }

                currentGame->move( startx, starty, endx, endy );
                successfulMove = true;
            }
        }   
        // If user has moved 2 squares diagonally in same direction
        // Most likely an attack move, but MUST validate it
        else if ( dispx == 2 )
        {
            // Move one square in the same direction to reference "jumped" piece
            int jumpedx = (endx - startx) / 2; 
            int jumpedy = (endy - starty) / 2;

            // Move one step in that direction
            jumpedx = startx + jumpedx; 
            jumpedy = starty + jumpedy;

            if ( currentGame->get_piece( jumpedx, jumpedy ) == NULL )
            {
                throw std::string("INVALID attack:- Must 'jump' another piece");
            }
            if ( currentGame->get_piece( jumpedx, jumpedy )->get_ownerID() == playernum )
            {
                throw std::string("INVALID attack:- Cannot 'jump' your own pieces");
            }
            else // Capture piece
            {
                // Remove jumped piece      
                currentGame->removePiece( jumpedx, jumpedy );

                // Increment players score  
                playList.at(playernum)->incrementScore( CAPTUREVALUE );
                
                // If the piece gets to the end, crown the piece
                if ( ( endy == 1 ) || ( endy == 8 ) )
                {
                    tempPiece->crown();
                }

                // Use standard move to move players piece to new location
                currentGame->move( startx, starty, endx, endy );

                // If the user isn't already on a captureStreak, they are now
                if ( !captureStreak )
                {
                    std::cout << "start capture streak" << std::endl;
                    captureStreak = true;
                }
                // If the user is already on a captureStreak, then they can continue their turn
                // NO ACTION REQUIRED
            }
        }
        else 
        {
            throw std::string("Move too large, please perform a single jump at a time. Player turns end after a move that DOESN'T result in a capture");
        }
    }

    // IF MOVE SUCCESSFUL swap players
    if ( successfulMove )
    {
        currentGame->swapPlayers();

        // Reset the flags
        successfulMove = false;
        captureStreak = false;
    }
}

int draughts::model::model::get_width()
{
    return currentGame->BOARDSIZE;
}

int draughts::model::model::get_height()
{
    return currentGame->BOARDSIZE;
}

// Virtual Destructor
draughts::model::model::~model(void)
{
}

draughts::model::model * draughts::model::model::get_instance(void)
{
    if(instance == nullptr)
    {
        instance = std::unique_ptr<model>(new model);   
    }
    return instance.get();    
}

void draughts::model::model::delete_instance(void)
{
    instance.reset(nullptr);
}