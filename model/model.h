/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Used to represent the rules for the game of draughts *
 ******************************************************************************/
#include <vector>
#include <memory>
#include <utility>
#include <iostream>
#include <sstream>
#include <map>
#include <thread>
#include <chrono>

#include "game.h"
#include "player.h"

#pragma once

namespace draughts
{
    namespace model
    {
        class game;

        class model
        {
            static std::unique_ptr<model> instance;

            game * currentGame;

            // Full player list
            std::vector<player *> playList;

            player * redPlayer = NULL;
            player * whitePlayer = NULL;

            const int CAPTUREVALUE = 1;

            // ID number of the next player to be added
            int nextID;

            /*
             * NOTE the following two flags MUST be class variables, as they 
             * need to persist through successive turns (ie if the user is on a 
             * captureStreak, then they will need to exit make_Move, and return
             * to get_input )
             * Flag to show the user has completed a successful move
             */
            bool successfulMove = false;
            /*
             * Flag to show that the user has just captured a piece. This means   
             * that their next move MUST also be a capture or their turn ends
             */
            bool captureStreak = false;

            model(void);
            
            bool player_exists(const std::string&);

            public:
            // Player functions
            void add_player(const std::string& );
            std::string get_player_name(int);
            int get_player_score(int);
            int get_current_player(void);
            std::string get_player_color( int playernum );
            std::map<int, std::string> get_player_list(void) const;

            // Game functions
            void start_game(int, int);
            int get_winner();
            char get_token(int,int);
            void make_move(int, int, int, int, int); 
            
            // Board functions
            int get_width();
            int get_height();

            virtual ~model(void);
            static model * get_instance(void);
            static void delete_instance(void);
        };

        // Needed to create the board
        class piece;
    }
}
