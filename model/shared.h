/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Header for changing colors used within this game     *
 ******************************************************************************/

#ifndef SHARED_H
#define SHARED_H

/* color codes required to display the tokens in color on the board */
#define COLOR_RED     "\33[31m"
#define COLOR_RESET   "\33[0m"

#endif /* defined SHARED_H */