/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Representation of a single game of draughts          *
 *                        Due to problem I trying to isolate the board methods *
 *                        into its own class, this class also contains board   *
 *                        specific variables and methods                       *
 ******************************************************************************/
#include "game.h"

std::unique_ptr<draughts::model::game> draughts::model::game::instance =
nullptr;

// Constructor
draughts::model::game::game( int player1, int player2 ) 
{
	// Take two player objects and assign to class variables
	currentPlayer = player1;
	nextPlayer = player2;

	// Initialize the board
	initBoard( player1, player2 );
}


draughts::model::game * draughts::model::game::get_instance(int player1, int player2)
{
    if(instance == nullptr)
    {
        instance = std::unique_ptr<game>(new game( player1, player2 ));	
    }
    return instance.get();    
}

// Initialize each of the squares with the empty squares set to NULL
// NO NEED TO ADJUST THE BOARD COORDINATES, as they are already set to 
// array coordinates
void draughts::model::game::initBoard( int player1, int player2 )
{
	for ( int y = 0; y < BOARDSIZE; y++ )
	{
		for ( int x = 0; x < BOARDSIZE; x++ )
		{
			// If the square is even
			if ( (x + y) % 2 == 0)
			{
				if ( y < 3 )
					gameBoard [y][x] = new piece( 'r', player1, 1 );
				else if ( y > (BOARDSIZE - 4) )
					gameBoard [y][x] = new piece( 'w', player2, -1 );
				else
					gameBoard [y][x] = NULL;
			}
			else
				gameBoard [y][x] = NULL;
		}
	}
}

/*
 * Used to get a specific token, from the array
 * Called from function of similar name within model.cpp
 */
char draughts::model::game::get_token_at( int x, int y )
{
	// Adjust board coordinates to suit array

	char tok;
	if ( gameBoard[ y - 1 ][ x - 1 ] == NULL )
		return ' ';
	else
	{
		tok = ( gameBoard[ y - 1 ][ x - 1 ]->get_symbol() ); 
		return tok;
	}
}

/*
 * Returns a reference to a specific piece, allowing the model to access
 * the piece object, including methods and properties, without public access
 * to the gameboard
 */
draughts::model::piece * draughts::model::game::get_piece( int x, int y )
{
	// Adjust board coordinates to suit array
	return gameBoard[ y - 1 ][ x - 1 ];
}

// For standard moves, without capturing
void draughts::model::game::move( int startx, int starty, int endx, int endy )
{
	// Adjust board coordinates to suit array
    startx -= 1;
    starty -= 1;
    endx   -= 1;
    endy   -= 1;

	// Move the player piece to the new location
	gameBoard[ endy ][ endx ] = gameBoard[ starty ][ startx ];

	// Clear previous space
	gameBoard[ starty ][ startx ] = NULL;
}

// Swap the currentPlayer and nextPlayer references
void draughts::model::game::swapPlayers( )
{
	int temp = currentPlayer;
	currentPlayer = nextPlayer;
	nextPlayer = temp;
}

// Return the currentPlayer reference ID
int draughts::model::game::get_current_player( void )
{
	return currentPlayer;
}

// Remove a piece from the gameboard
void draughts::model::game::removePiece( int x, int y )
{
	delete gameBoard[ y - 1 ][ x - 1 ];
	gameBoard[ y - 1 ][ x - 1 ] = NULL;
}