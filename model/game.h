/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Representation of a single game of draughts          *
 *                        Due to problem I trying to isolate the board methods *
 *                        into its own class, this class also contains board   *
 *                        specific variables and methods                       *
 ******************************************************************************/
#include "piece.h"

#include <memory>

#pragma once

namespace draughts
{
    namespace model
    {
        class game
        {
        	static std::unique_ptr<game> instance;

            int currentPlayer, nextPlayer;

            // Constructor
            game(int, int);

            public:
            // Used for width and height as we want a square board of 8 spaces
            static const int BOARDSIZE = 8;

            // BOARD REFERENCE
            draughts::model::piece * gameBoard [BOARDSIZE] [BOARDSIZE];
            
            static game * get_instance(int, int);

            void initBoard( int player1, int player2 );

            char get_token_at( int x, int y );

            piece * get_piece( int x, int y );

            void move( int startx, int starty, int endx, int endy );

            void swapPlayers( void );

            int get_current_player( void );

            void removePiece( int x, int y );

            // void endGame(void);
        };
    }
}