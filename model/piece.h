/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * File Author          : Cameron Watt                                         *
 * File Description     : Implementation of the model used in the MVC pattern  *
 *                        Representation of a single piece in a draughts game  *
 ******************************************************************************/

#pragma once

namespace draughts
{
    namespace model
    {
        class piece
        {
            bool crowned;

            char token;

            // Player ID of the "owner" of this piece
            int ownerID;

            // Players can be either -1 or 1
            // 1 if they are moving from the top of the board to the bottom
            // -1 if they are moving from the bottom of the board to the top
            // If they are crowned, this value is IGNORED
            int direction;

            public:
            piece( char _token, int playerNo, int direction );

            char get_symbol(void);

            void crown(void);

            int get_ownerID( void );

            int get_direction( void );

            bool check_crowned( void );
        };
    }
}