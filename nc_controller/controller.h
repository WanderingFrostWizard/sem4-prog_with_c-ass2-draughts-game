/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Header for the controller used in the MVC pattern    *
 *                        This file is used to execute the various commands    *
 *                        used throughout the game                             *
 ******************************************************************************/
#include <memory>
#include <vector>
#include "command.h"
#include <chrono>
#include <thread>

#pragma once

namespace draughts
{
    namespace ncview
    {
        class ui;
    }
    namespace model
    {
        class model;
    }
    namespace nc_controller
    {
        class command;

        class controller
        {
            static std::unique_ptr<controller>instance;

            model::model * get_model(void);
            ncview::ui * get_view(void);
            controller(void);

            public:
            static controller* get_instance(void);
            std::vector<std::unique_ptr<command>> 
                get_main_menu_commands(void);
            void start_game(std::map<int, std::string>);
            static void delete_instance(void);
            virtual ~controller(void);

        };
    }
}
