/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Implementation of the controller used in the MVC     *
 *                        pattern.                                             *
 *                        This file is used to execute the various commands    *
 *                        used throughout the game                             *
 ******************************************************************************/
#include "command.h"
#include "../ncview/ui.h"
draughts::ncview::ui * draughts::nc_controller::command::view = 
draughts::ncview::ui::get_instance();
