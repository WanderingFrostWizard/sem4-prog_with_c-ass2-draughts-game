/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Part of the view used in the MVC pattern             *
 *                        This file is a generic window creation class. It     *
 *                        manages the user input to this window, validation of *
 *                        this input, and a custom exception if needed         *
 ******************************************************************************/
#include "window.h"
#include "ui.h"
#include "../model/model.h"
#include "../nc_controller/command.h"

draughts::ncview::window::window(void) 
    : view(draughts::ncview::ui::get_instance()), 
    themodel(draughts::model::model::get_instance())
{
}

std::string draughts::ncview::window::get_input(
        const std::string& prompt)
{
    std::string input;
    bool success = false;
    while(!success){
        std::cout << prompt + ": ";
        try{
            std::getline(std::cin, input);
            if(input.empty()){
                if(std::cin.eof()){
                    std::cin.clear();
                }
                throw input_cancelled();
            }
            success = true;
        }
        catch(input_cancelled & ic)
        {
            throw;
        }
    }
    return input;
}
