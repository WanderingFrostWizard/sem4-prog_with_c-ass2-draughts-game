/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Part of the view used in the MVC pattern             *
 *                        This file is used to display the main menu of the    *
 *                        game                                                 *
 ******************************************************************************/
#include "menu.h"

#pragma once

namespace draughts
{
    namespace ncview
    {
        class main_menu : public menu
        {
            static const std::vector<std::string> strings;
            public:
                main_menu(void);
                virtual ~main_menu(void);
        };
    }
}
