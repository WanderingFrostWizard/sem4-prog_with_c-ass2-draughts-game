/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Part of the view used in the MVC pattern             *
 *                        This file is used to display the player selection    *
 *                        menu of the game, as well as manage the input and    *
 *                        output used in this window                           *
 ******************************************************************************/
#include "window.h"
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <sstream>
#include "menu.h"

#pragma once

namespace draughts
{
    namespace nc_controller
    {
        class controller;
    }
    namespace ncview
    {
        using player_map = std::map<int, std::string>;
        const int NUM_PLAYERS = 2;
        class player_selection_window : public window
        {
            const std::map<int,std::string> full_list;
            std::map<int,std::string> selected_list;
            public:
            player_selection_window(const player_map&);
            virtual void activate(void) override;
            std::string players_to_string(void);
            static std::vector<std::string> player_strings(const player_map&, 
                player_map&);
            static std::vector<std::unique_ptr<nc_controller::command>>
                create_actions(const player_map&, player_map&);
            virtual ~player_selection_window(void){}
        };
    }
}
