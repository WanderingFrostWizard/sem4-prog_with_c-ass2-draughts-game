/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Part of the view used in the MVC pattern             *
 *                        This file is used to display the game                *
 ******************************************************************************/
#include "game_window.h"
#include "../model/model.h"

#include "../model/shared.h"

draughts::ncview::game_window::game_window(const player_pair & theplayers) 
    : players(theplayers), quit(false)
{
}

void draughts::ncview::game_window::activate(void) 
{
    while(!quit)
    {
        int playernum = EOF;
        try
        {
            display_board();
            playernum = themodel->get_current_player();
            std::cout << "it is " << themodel->get_player_name(playernum)
                << "'s turn and their score is " 
                << themodel->get_player_score(playernum) << std::endl;
        }
        catch(std::exception & ex)
        {
            std::cerr << ex.what() << std::endl;
            return;
        }
        try
        {
            std::pair<std::pair<int,int>,std::pair<int,int>> move_coords;
            move_coords = get_move_input();
            themodel->make_move(playernum, move_coords.first.first, 
                move_coords.first.second, move_coords.second.first,
                move_coords.second.second);

            std::cout << move_coords.first.first << "," << move_coords.first.second << " - ";
            std::cout << move_coords.second.first << "," << move_coords.second.second << std::endl;
        }
        catch(std::exception& ex)
        {
            std::cerr << ex.what() << std::endl;
        }
        catch( std::string errorMessage )
        {
            std::cerr << "ERROR: " << errorMessage << std::endl;
        }
    }
}

std::pair<std::pair<int,int>, std::pair<int,int>> 
    draughts::ncview::game_window::get_move_input(void)
{
    std::string input;
    std::pair<std::pair<int,int>,std::pair<int,int>> move;
    std::pair<int,int> start;
    std::pair<int,int> end;
    std::cout << "Please enter your next move: " ;

    int currPlayerID = themodel->get_current_player();
    
    input = get_input( themodel->get_player_name( currPlayerID ) + " you are " + themodel->get_player_color(currPlayerID) );

    if ( input.find("-") == std::string::npos )
    {
        throw std::string("invalid coordinates");
    }     

    std::vector<std::string> moves;
    // std::vector<std::string> coords;

    boost::split(moves, input, [](char ch){return ch == '-';});

    start = strtocoord(moves[0]);
    end = strtocoord(moves[1]);
    move = std::make_pair(start, end);
    return move;
}

std::pair<int,int> draughts::ncview::game_window::strtocoord(
    const std::string& input)
{
    int x, y;
    std::vector<std::string> parts;
    boost::split(parts, input, [](char ch){ return ch == ',';});
    x = stoi(parts[0]);
    y = stoi(parts[1]);
    return std::make_pair(x,y);
}

void draughts::ncview::game_window::print_top_row(void)
{
    int xcount;
    std::cout << " ";
    for (xcount = 0; xcount < themodel->get_width(); ++xcount)
    {
        std::cout << " " << xcount + 1 << " |";
    }
    std::cout << std::endl;
}

/* EDITED TO SHOW COLORED PIECES
 * NOTE THAT THIS FUNCTION'S COORDINATES DO NOT NEED TO BE MODIFIED, as they 
 * are currently requesting coordinates between 1,1 and 8,8, which is what 
 * the board within the game class is expecting
 */
void draughts::ncview::game_window::print_row(int rownum)
{
    int xcount;
    char token;
    std::string output;

    std::cout << rownum + 1;
    for(xcount = 0; xcount < themodel->get_width(); ++xcount)
    {
        token = themodel->get_token(xcount + 1, rownum + 1);
        output = "";

        if ( (token == 'r') || (token == 'R') )
            output.append( COLOR_RED );

        output.append( 1, token );
        output.append( COLOR_RESET );

        std::cout << " " << output << " |";
    }
    std::cout << std::endl;
}

void draughts::ncview::game_window::print_line(int numdashes)
{
    int count;
    for(count = 0; count < numdashes; ++count)
    {
        std::cout << '-';
    }
}

void draughts::ncview::game_window::display_board(void)
{
    int ycount;

    print_top_row();
    print_line(themodel->get_width() * 4);
    std::cout << std::endl;

    for(ycount = 0; ycount < themodel->get_height(); ycount++)
    {
        print_row(ycount);
        print_line(themodel->get_width() * 4);
        std::cout << std::endl;
    }
}