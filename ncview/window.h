/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Part of the view used in the MVC pattern             *
 *                        This file is a generic window creation class. It     *
 *                        manages the user input to this window, validation of *
 *                        this input, and a custom exception if needed         *
 ******************************************************************************/
#include <vector>
#include <memory>
#include <exception>

#pragma once

namespace draughts
{
    namespace nc_controller
    {
        class command;
        class controller;
    }
    namespace model
    {
        class model;
    }
    namespace ncview
    {
        class ui;
        class input_cancelled : public std::exception
        {
            public:
                virtual const char * what(void) const noexcept override
                {
                    return "input was cancelled";
                }

                virtual ~input_cancelled(void){}
        };
        using command_vector = std::vector<std::unique_ptr<
            nc_controller::command>>;
        class window
        {
            protected: 
                ncview::ui * view;
                model::model * themodel;
                nc_controller::controller * thecontroller;
            public:
                window(void);
                virtual void activate(void) = 0;
                static std::string get_input(const std::string&);
                virtual ~window(void){}
        };
    }
}
