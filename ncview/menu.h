/*******************************************************************************
 * Semester 2 2017 Assignment #2 - September - October 2017                    *
 * Project completed by : Cameron Watt and Bryan Chu Chait Soo                 *
 * Student Number       : s3589163 and s3441433                                *
 * Course Code          : COSC1254 - Programming using C++                     *
 * Original Author      : Paul Miller (provided source code)                   *
 * Modified by          : Cameron Watt                                         *
 * File Description     : Part of the view used in the MVC pattern             *
 *                        This file is used to display the menus of the game   *
 ******************************************************************************/
#include "window.h"
#include <vector>
#include <memory>
#include <string>
#include <iostream>

#pragma once

namespace draughts
{
    namespace ncview
    {
        class menu : public window
        {
            std::string title;
            std::vector<std::string> menu_text;
            command_vector menu_commands;
            public:
                menu(const std::string&, const std::vector<std::string>&, 
                    command_vector);
                menu(void);
                void set_title(const std::string&);
                void set_text(const std::vector<std::string>&);
                void set_actions(command_vector&);
                virtual void activate(void) override;
                virtual ~menu(void){}
        };
    }
}
